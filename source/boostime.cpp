#include <iostream>
#include "boost/date_time/posix_time/posix_time.hpp"

using namespace boost::posix_time;
using namespace boost::gregorian;

int main ( int argc, char** argv){



    //get the current time from the clock -- one second resolution
    ptime now = microsec_clock::universal_time();

std::string timeUTC = to_iso_extended_string(now);

	timeUTC.erase(timeUTC.size()-3);	


    std::cout << "Current timestamp: "
              << to_simple_string(now) << " ... " << 
		timeUTC
		 << std::endl;
	return 0;
}
