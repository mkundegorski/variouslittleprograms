#include <iostream>
#include <time.h>
#include <cv.h>
#include <highgui.h>
#include "tinyxml2.h"


using namespace cv;
using namespace tinyxml2;

int main(int argc, char** argv){



//hard-coded gps tracklet saving:
	/**********************************/
	//starts here
	/********************************/
	
	//name
	std::string mName;
	mName = "miks.xml";


	XMLDocument generalXml;
	//generalXml.LoadFile(mName.c_str()); silently crashes when file does not exist

	generalXml.Clear();
	XMLDeclaration* topdec = generalXml.NewDeclaration(); //<?xml version="1.0" ...

/*
	XMLElement* gpxNode = generalXml.NewElement("gpx");
	gpxNode->SetAttribute("version","1.0");
	gpxNode->SetAttribute("creator","miko");
	gpxNode->SetAttribute("xmlns:xsi","www.dur.ac.uk");
	gpxNode->SetAttribute("xmlns","www.dur.ac.uk");
	gpxNode->SetAttribute("xsi:schemaLocation","http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd");
*/
	XMLElement* detNode = generalXml.NewElement("DetectionReport");

	XMLElement* timenode = generalXml.NewElement("timestamp");
	XMLText* timetxt = generalXml.NewText("2014-07-24T12:01:23.000Z");
	timenode->InsertFirstChild(timetxt);


	XMLElement* sourceID = generalXml.NewElement("sourceID");
	XMLText* sourceT = generalXml.NewText("0");
	sourceID->InsertFirstChild(sourceT);

	
	detNode->InsertFirstChild(sourceID);
	detNode->InsertFirstChild(timenode);

	//Track informations
	XMLElement* trackInfo1 = generalXml.NewElement("trackInfo");
	trackInfo1->SetAttribute("type","confidence");
	trackInfo1->SetAttribute("value",0.9);
	trackInfo1->SetAttribute("e",0.01);

	detNode->InsertEndChild(trackInfo1);

	XMLElement* trackInfo2 = generalXml.NewElement("trackInfo");
	trackInfo2->SetAttribute("type","speed");
	trackInfo2->SetAttribute("value",2.0);
	trackInfo2->SetAttribute("e",0.5);

	detNode->InsertEndChild(trackInfo2);



	generalXml.InsertEndChild(detNode);
	generalXml.SaveFile(mName.c_str());
	/**********************************/
	//and ends here
	/********************************/
	
	return 0;

}
