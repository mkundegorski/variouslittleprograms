// Definition of the ClientSocket class

#ifndef ClientSocket_class
#define ClientSocket_class

#include "Socket.h"


class ClientSocket : private Socket
{
 public:

  ClientSocket ( std::string host, int port );
  virtual ~ClientSocket(){};

	void set_nb(){
		set_non_blocking (true);
	}

	bool poll_socket(){
		bool out;
		out = poll();
		std::cout << "out\n";
		return out;
	}
		


  const ClientSocket& operator << ( const std::string& ) const;
  const ClientSocket& operator >> ( std::string& ) const;

};


#endif
