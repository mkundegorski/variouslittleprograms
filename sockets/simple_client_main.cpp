#include "ClientSocket.h"
#include "SocketException.h"
#include <iostream>
#include <string>

int main ( int argc, int argv[] )
{
  try
    {

      ClientSocket client_socket ( "localhost", 30000 );
      std::cout << "Setting nonblocking \n";
	client_socket.set_nb();

      std::string reply;

      try
	{
	  client_socket << "Test message.";
	  //char a;
	  //std::cin >> a;
	}
      catch ( SocketException& ) {}

     // std::cout << "We received this response from the server:\n\"" << reply << "\"\n";;

	  while(1){
		      try
		{
			
			std::cout << "start main loop\n";
			bool pollres = false;
			pollres = client_socket.poll_socket();

			std::cout << "pollres: " << pollres << "\n";

			if(pollres) { //port polling... 
				reply = "polling string";
				std::cout << "there is a data to read\n";
				client_socket >> reply;
		  		std::cout << "and it says: " << reply << "(end of message)\n";
			}
		}
			catch ( SocketException& ) {
	      
		  std::cout << "exception\n";
	      
	      }
		
		  //std::cout << ". " << reply << "\n";

	  }

    }
  catch ( SocketException& e )
    {
      std::cout << "Exception was caught:" << e.description() << "\n";
    }

  return 0;
}
